import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { ScanComponent } from './components/scan/scan.component';
import { SendComponent } from './components/send/send.component';
import { ConfirmComponent } from './components/send/confirm.component';
import { ReceiveComponent } from './components/receive/receive.component';
import { TransactionHistoryComponent } from './components/transaction-history/transaction-history.component';

const routes: Routes = [
  { path: "", component: MainComponent },
  { path: "scan", component: ScanComponent },
  { path: "send", component: SendComponent },
  { path: "confirm-send", component: ConfirmComponent },
  { path: "receive", component: ReceiveComponent },
  { path: "history", component: TransactionHistoryComponent },
  { path: "", redirectTo: "/", pathMatch: "full" },
  { path: "**", redirectTo: "/", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
