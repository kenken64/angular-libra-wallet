import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Fablocation } from "./components/bottom-bar/bottom-bar.component";
import {
  PerfectScrollbarConfigInterface,
  PerfectScrollbarComponent,
  PerfectScrollbarDirective
} from "ngx-perfect-scrollbar";
import { Router } from "@angular/router";
import { AuthService } from "./services/auth.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AngularFireAuth } from "@angular/fire/auth";
import { Subscription } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { WalletService } from "./services/wallet.service";
import { MatSnackBar } from "@angular/material";
import { MatBottomSheetRef, MatBottomSheet } from '@angular/material';
import { BleService } from './services/ble.service';
import { map } from "rxjs/operators";
import { Wallet } from "./models/Wallet";

@Component({
  selector: "filter-language",
  templateUrl: "lang-selector.html"
})
export class BottomSheetLanguageSheet {
  constructor(
    private bottomSheetRef: MatBottomSheetRef<BottomSheetLanguageSheet>,
    private translate: TranslateService
  ) {}

  selectLang(event: MouseEvent, indicator: string): void {
    if (indicator === "en") {
      this.translate.setDefaultLang("en");
    }
    if (indicator == "zh") {
      this.translate.setDefaultLang("zh");
    }
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy {
  title = "angular-libra-wallet";
  insetToolbar = true;
  fabLocation = Fablocation.CENTER;
  color = "primary";
  flat = false;
  public type: string = 'component';
  isHWWalletConnected = false;

  public disabled: boolean = false;
  public config: PerfectScrollbarConfigInterface = {};

  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective)
  directiveRef?: PerfectScrollbarDirective;
  PASSWORD_PATTERN = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{12,}$/;
  loginForm: FormGroup;
  registerForm: FormGroup;
  private loginWithEmailSub: Subscription;
  isLoading = false;
  valuesSubscription: Subscription;
  streamSubscription: Subscription;
  deviceSubscription: Subscription;
  accountAlreadyActivated = false;
  doNotShow = true;
  doNotShowRegister = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    public afAuth: AngularFireAuth,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private walletSvc: WalletService,
    private bleSvc: BleService,
    private bottomSheet: MatBottomSheet){
      this.loginForm = fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern(this.PASSWORD_PATTERN)]],
      });

      this.registerForm = fb.group({
        email: ["", [Validators.required, Validators.email]],
        password: [
          "",
          [Validators.required, Validators.pattern(this.PASSWORD_PATTERN)]
        ],
        firstName: ["", [Validators.required, Validators.minLength(3)]],
        lastName: ["", [Validators.required, Validators.minLength(3)]]
      });
  
      translate.setDefaultLang('en');
  }

  ngOnInit() {

    if(typeof(this.authService.getEmail()) !== 'undefined'){
      this.walletSvc
      .getWalletByEmail()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(wallet => {
        console.log(wallet[0].address);
        if (!wallet[0].address && !wallet[0].mnemonic) {
          console.log("already activated");
          this.accountAlreadyActivated = true;
        }
      });
    }
  }

  switchLang(): void {
    this.bottomSheet.open(BottomSheetLanguageSheet);
  }
  public onScrollEvent(event: any): void {}

  scan() {
    this.router.navigate(["/scan"]);
  }

  ngOnDestroy() {
    if (typeof this.loginWithEmailSub !== "undefined") {
      this.loginWithEmailSub.unsubscribe();
    }
  }

  loginWithEmail() {
    const formValue = this.loginForm.value;
    this.isLoading = true;
    try {
      this.loginWithEmailSub = this.authService
        .loginWithEmail(formValue.email, formValue.password)
        .subscribe(
          result => {
            this.authService.setFirebaseTokenToLocalstorage();

            setTimeout(
              function() {
                this.isLoading = false;
              }.bind(this),
              5000
            );
          },
          error => {
            this.isLoading = false; // error path
          }
        );
    } catch (e) {
      this.isLoading = false;
    }
  }

  registerWithEmail() {
    this.walletSvc.createWallet().then(result => {
      const formValue = this.registerForm.value;
      console.log(result);
      let w: Wallet = {
        email: String(formValue.email),
        mnemonic: result.mnemonic,
        address: result.address,
        firstName: formValue.firstName,
        lastName: formValue.lastName
      };
      this.authService.registerWithEmail(formValue.email, formValue.password).subscribe((result)=>{
        setTimeout(
          function() {
          }.bind(this),
          4000
        );
        console.log(result);
        this.walletSvc.saveWallet(w).subscribe(result => {
          console.log(">>>> " + result);
          let snackBarRef = this.snackBar.open("Wallet created", "done", {
            duration: 3000
          });
        });
      })
      
    });
  }

  logout() {
    this.isLoading = false;
    this.afAuth.auth.signOut().then(result => {
      console.log(result);
      this.authService.destroyToken();
      this.doNotShow = true;
      this.doNotShowRegister = false;
    });
  }

  openAccount() {
    this.doNotShow = false;
    this.doNotShowRegister = true;
  }

  cancelCreate(){
    this.doNotShow = true;
    this.doNotShowRegister = false;
  }

  openHwWallet(){
    let serviceUuid = 'c03e7090-7ce0-46f0-98dd-a2aba8367741'
    let characteristicUuid = '26e2b12b-85f0-4f3f-9fdd-91d114270e6e'
    let device = null
    let resultfromHW = ''
    try{ 
      
    } catch (error) {
        console.log('Argh! ' + error)
    }
  }

  disconnectHwWallet(){
    console.log("disconnect Hw wallet");
    this.bleSvc.disconnectDevice();
    this.deviceSubscription.unsubscribe();
    this.valuesSubscription.unsubscribe();
  }
}
