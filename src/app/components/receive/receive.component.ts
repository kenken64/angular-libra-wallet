import { Component, OnInit } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard'
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { WalletService } from "../../services/wallet.service";
import { map } from "rxjs/operators";

@Component({
  selector: 'app-receive',
  templateUrl: './receive.component.html',
  styleUrls: ['./receive.component.css']
})
export class ReceiveComponent implements OnInit {
  value: any;
  constructor(private _clipboardService: ClipboardService,
    private _snackBar: MatSnackBar,
    private translator: TranslateService,
    private walletsvc: WalletService) { }

  ngOnInit() {
    this.walletsvc
      .getWalletByEmail()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(wallet => {
        this.value = wallet[0].address;
      });
  }

  copyToClipboard(){
    this.translator.get('Copied').subscribe((res: string) => {
      let arrResult = res.split('|');
      this._clipboardService.copyFromContent(this.value);
      this._snackBar.open(arrResult[0], arrResult[1], {
        duration: 3000,
      });
    });
  }

  rotateKey(){

  }
}
