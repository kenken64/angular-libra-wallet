import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WalletService } from '../../services/wallet.service';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css']
})
export class SendComponent implements OnInit {
  sendForm :FormGroup;
  isLoading: boolean =  false;
  amount_pattern= /^[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*$/;
  
  constructor(private fb: FormBuilder, private router: Router,
    private walletsvc: WalletService) { 
    this.sendForm = fb.group({
      destAccount: ['', [Validators.required]],
      amount: ['', [Validators.required , Validators.pattern(this.amount_pattern)]],
    });
  }

  ngOnInit() {
    let scanAddress = this.walletsvc.scanAddresss;
    console.log("SCAN ADDRESS >>> " + scanAddress);
    if(typeof(scanAddress) !== 'undefined' ){
      this.sendForm.patchValue({
        destAccount: scanAddress
      });
    }
  }

  get fc() { return this.sendForm.controls; }

  onSend(){
    this.isLoading = true;
    let destAccount = this.sendForm.get("destAccount").value;
    let amount = this.sendForm.get("amount").value;
    this.walletsvc
      .getWalletByEmail()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(wallet => {
        console.log(wallet[0].address);
        let p = {
          fromAddress: wallet[0].address,
          mnemonic: wallet[0].mnemonic,
          toAddress: destAccount,
          amount: amount
        }
        
        this.walletsvc.transfer(p).then(result=>{
          console.log(result);
          this.walletsvc.confirmSent(result);
          this.isLoading = false;
          this.router.navigate(['/confirm-send']);
        });
      });
    
  }
}
