import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { WalletService } from "../../services/wallet.service";
import { TextAnimation } from "ngx-teximate";
import { rotateIn } from "ng-animate";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-confirm",
  templateUrl: "./confirm.component.html",
  styleUrls: ["./confirm.component.css"]
})
export class ConfirmComponent implements OnInit {
  routeData: any;
  message: string;
  enterAnimation: TextAnimation = {
    animation: rotateIn,
    delay: 50,
    type: "letter"
  };
  constructor(private router: Router, 
    private walletsvc: WalletService,
    private translator:TranslateService) {}

  ngOnInit() {
    this.routeData = this.walletsvc.confirmation;
    console.log(this.routeData);
    this.translator.get('Libra sent.').subscribe((res: string) => {
      this.message = `${this.routeData.amount} ${res}`;
    });
    
  }

  navigateToHistory() {
    this.router.navigate(["/history"]);
  }
}
