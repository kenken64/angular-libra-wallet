import { Component, OnInit, OnDestroy } from "@angular/core";
import { WalletService } from "../../services/wallet.service";
import { Subscription } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-transaction-history",
  templateUrl: "./transaction-history.component.html",
  styleUrls: ["./transaction-history.component.css"]
})
export class TransactionHistoryComponent implements OnInit, OnDestroy {
  transactionHistory: any;
  libraAccount: any;
  constructor(private walletsvc: WalletService) {}
  private walletSub: Subscription;

  ngOnDestroy() {
    this.walletSub.unsubscribe();
  }

  ngOnInit() {
    this.walletSub = this.walletsvc
      .getWalletByEmail()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(wallet => {
        console.log(wallet[0].address);
        this.libraAccount = wallet[0].address;
        let p = {
          address: this.libraAccount
        };
        this.walletsvc.getTransactionHistory(p).then(result => {
          console.log(result);
          this.transactionHistory = result;
          console.log(this.transactionHistory);
        });
      });
    
  }
}
