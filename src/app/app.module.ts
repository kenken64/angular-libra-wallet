import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScanComponent } from './components/scan/scan.component';
import { SendComponent } from './components/send/send.component';
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component';
import { MainComponent } from './components/main/main.component';
import { TransactionHistoryComponent } from './components/transaction-history/transaction-history.component';
import { ReceiveComponent } from './components/receive/receive.component';
import { ConfirmComponent } from './components/send/confirm.component';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from "@angular/forms";

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { ClipboardModule } from 'ngx-clipboard';
import { TeximateModule } from 'ngx-teximate';

import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";

import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireMessagingModule } from "@angular/fire/messaging";

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { WebBluetoothModule } from '@manekinekko/angular-web-bluetooth';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any> {
    swipe: { direction: Hammer.DIRECTION_ALL },
  };
}

import { AuthService } from "./services/auth.service";
import { AuthInterceptor } from './services/auth.interceptor';
import { BottomSheetLanguageSheet } from './app.component';
import { LocalizedDatePipe } from './shared/localized-date.pipe';

import { registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeZh from '@angular/common/locales/zh';
import localeZhExtra from '@angular/common/locales/extra/he';

registerLocaleData(localeEn, 'en');
registerLocaleData(localeZh, 'zh', localeZhExtra);


@NgModule({
  entryComponents: [BottomSheetLanguageSheet],
  declarations: [
    AppComponent,
    ScanComponent,
    SendComponent,
    TransactionHistoryComponent,
    ReceiveComponent,
    BottomBarComponent,
    MainComponent,
    ConfirmComponent,
    BottomSheetLanguageSheet,
    LocalizedDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    FormsModule,
    HttpClientModule,
    NgxQRCodeModule,
    ZXingScannerModule,
    ClipboardModule,
    TeximateModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireMessagingModule,
    AngularFireAuthModule,
    WebBluetoothModule.forRoot({
      enableTracing: true
    }),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  providers: [
    AngularFirestore,
    AuthService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}