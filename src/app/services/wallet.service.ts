import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { of } from "rxjs";
import { AuthService } from './auth.service';
import { Wallet } from '../models/Wallet';

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  walletRef$: AngularFireList<Wallet[]>;
  singleWalletRef$: AngularFireList<Wallet>;
  private dbPath = "/wallet";
  public scanAddresss: any;
  public confirmation: any;

  constructor(private http: HttpClient,
    private db: AngularFireDatabase,
    private authSvc : AuthService,) { 
      this.walletRef$ = this.db.list(this.dbPath);
  }

  getBalance(address: any): Promise<any>{
    console.log(address);
    return (
      this.http.post<any>(`${environment.api_url}/getBalance`, address).toPromise()
    );
  }

  getTransactionHistory(address: any): Promise<any>{
    console.log(address);
    return (
      this.http.post<any>(`${environment.api_url}/transactionHistory`, address).toPromise()
    );
  }

  transfer(fund: any): Promise<any>{
    console.log(fund);
    return (
      this.http.post<any>(`${environment.api_url}/transfer`, fund).toPromise()
    );
  }

  createWallet(){
    return (
      this.http.post<any>(`${environment.api_url}/createWallet`, {}).toPromise()
    );
  }

  saveWallet(wallet) {
    return of(this.walletRef$.push(wallet));
  }

  confirmSent(p:any){
    this.confirmation = p;
  }

  setScanAddress(address:any){
    this.scanAddresss = address;
  }
  
  getWalletByEmail(): AngularFireList<Wallet> {
    this.singleWalletRef$ = this.db.list(this.dbPath, ref =>
      ref
        .orderByChild("email")
        .equalTo(this.authSvc.getEmail().toString())
    );
    return this.singleWalletRef$;
  }

}
