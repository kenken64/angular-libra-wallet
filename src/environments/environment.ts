// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  libra_account: 'f4b79df549f1d1922bf2b170b97febc098e3cadef427f3b2646e8bff54cfa92a',
  mnemonic: 'bleak piano damp benefit swim that pass battle exercise maple quarter sausage organ crawl patch episode argue embark biology ostrich claw dad arrive cause',
  api_url: 'https://b1f08b51.jp.ngrok.io',
  firebase: {
    apiKey: "AIzaSyDeca5K26DxHVzrAUpVsEgj2kjTM_7q_SI",
    authDomain: "libra-wallet-28877.firebaseapp.com",
    databaseURL: "https://libra-wallet-28877.firebaseio.com",
    projectId: "libra-wallet-28877",
    storageBucket: "libra-wallet-28877.appspot.com",
    messagingSenderId: "903414187806",
    appId: "1:903414187806:web:4e1f95dad2a1f0b0782b0f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
