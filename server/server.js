require("dotenv").config();
const express = require("express"),
  cors = require("cors"),
  bodyParser = require("body-parser"),
  BigNumber = require("bignumber.js"),
  { LibraClient, LibraWallet, LibraNetwork } = require("kulap-libra"),
  axios = require("axios"),
  morgan = require('morgan'),
  moment = require("moment");

const app = express();
app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(express.static(__dirname + '/public'));

app.post("/getBalance", async (req, res) => {
  let address = req.body.address;
  const client = new LibraClient({ network: LibraNetwork.Testnet });
  const accountState = await client.getAccountState(address);
  // balance in micro libras
  const balanceInMicroLibras = BigNumber(accountState.balance.toString(10));
  // balance in base unit
  const balance = balanceInMicroLibras.dividedBy(BigNumber(1e6));
  res.status(200).json(balance.toString(10));
});

app.post("/createWallet", (req, res) => {
  // Generate account
  const wallet = new LibraWallet();
  const account = wallet.newAccount();

  res.status(200).json({
    address: account.getAddress().toHex(),
    mnemonic: wallet.config.mnemonic
  });
});

app.post("/transfer", async (req, res) => {
  let mnemonic = req.body.mnemonic;
  let toAddress = req.body.toAddress;
  let amount = req.body.amount;
  const client = new LibraClient({ network: LibraNetwork.Testnet });
  const wallet = new LibraWallet({ mnemonic: mnemonic }); // Load wallet from mnemonic phrase BIP39
  //const account = wallet.generateAccount(0) // Derivation paths to "LIBRA WALLET: derived key$0"
  const account = wallet.newAccount();
  const amountToTransfer = BigNumber(amount).times(1e6); // Amount in micro libras

  // Stamp account state before transfering
  await client.getAccountState(account.getAddress());
  console.log(account);
  console.log(toAddress);
  console.log(amountToTransfer);
  
  // Transfer
  const response = await client.transferCoins(
    account,
    toAddress,
    amountToTransfer
  );

  // temporary wait 2 seconds for consensus
  await new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, 3000);
  });
  console.log(response);
  res.status(200).json({
    response: response,
    address: account.getAddress().toHex(),
    amount: amount
  });
});

app.post("/mint", async (req, res) => {
  let address = req.body.address;
  let amount = req.body.amount;
  console.log("transaction history");
  // On Node
  const client = new LibraClient({ network: LibraNetwork.Testnet });

  const account = wallet.newAccount();

  // mint 2 libracoins to users accounts
  await client.mintWithFaucetService(account.getAddress(), amount);
  res.status(200).json({
    address: address,
    status: "successful"
  });
});

app.post("/transactionHistory", async (req, res) => {
  let address = req.body.address;
  // Get transaction histories from libexplorer
  const url = `${process.env.LIBRA_EXPLORER_URL}${address}`;
  console.log(`calling ${url}`);
  const response = await axios
    .get(url, { timeout: 10000 })
    .then(resp => resp)
    .catch(error => ({ error: error.message }));

  // Valdiate response
  if (response.error || response.data.status !== "1") {
    if (response.data.status === "0") {
      // Case of Invalid address format
      if (response.data.message === "NOTOK") {
        throw new Error(response.data.result);

        // Case of No transactions found
      } else if (response.data.message === "No transactions found") {
        return [];
      }
    }

    const msg = response.error ? response.error : JSON.stringify(response.data);
    console.error(`Failed response ${msg}`);
    throw new Error(`Internal server error`);
  }

  // Transform data
  let transactions = response.data.result.map(transaction => {
    // Convert from micro libras
    const amountInBaseUnit = BigNumber(transaction.value).div(1e6);
    let output = {
      amount: amountInBaseUnit.toString(10),
      fromAddress: transaction.from,
      toAddress: transaction.to,
      date: moment.utc(parseInt(transaction.expirationTime) * 1000).format(),
      transactionVersion: parseInt(transaction.version),
      explorerLink: `${process.env.LIBRA_EXPLORER_LINK}${transaction.version}`
    };
    // Mint
    if (
      transaction.from ===
      "0000000000000000000000000000000000000000000000000000000000000000"
    ) {
      output.event = "mint";
      output.type = "mint_transaction";
      // Sent
    } else if (transaction.from.toLowerCase() === address.toLowerCase()) {
      output.event = "sent";
      output.type = "peer_to_peer_transaction";
      // Received
    } else {
      output.event = "received";
      output.type = "peer_to_peer_transaction";
    }
    return output;
  });

  // Sort by transaction version desc
  transactions = transactions.sort((a, b) => {
    return b.transactionVersion - a.transactionVersion;
  });

  res.status(200).json(transactions);
});

app.post("/rotate-key", async (req, res) => {
  let mnemonic = req.body.mnemonic;
  const client = new LibraClient({ network: LibraNetwork.Testnet });
  const wallet = new LibraWallet({
    mnemonic: mnemonic
  });
  const account = wallet.newAccount();
  const account2 = wallet.newAccont();
  const account2Address = account2.getAddress().toHex();
  const response = await client.rotateKey(account1, account2Address);
  const accountState = await client.getAccountState(account.getAddress());
  const balanceInMicroLibras = BigNumber(accountState.balance.toString(10));
  const balance = balanceInMicroLibras.dividedBy(BigNumber(1e6));
  // transfer coin with newly updated key
  const response2 = await client.transferCoins(
    account1,
    account2Address,
    balance,
    account2.keyPair
  );
  res.status(200).json({
    rotate: response,
    transfer: response2
  });
});

const PORT_NUMBER = process.env.PORT;

app.listen(PORT_NUMBER, function() {
  console.log(`Libra App Server is running on port ${PORT_NUMBER}`);
});
