function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div\r\n  fxLayout=\"column\"\r\n  fxFlexFill\r\n  *ngIf=\"afAuth.user | async as user; else showLogin\"\r\n>\r\n  <mat-toolbar color=\"primary\">\r\n    <img\r\n      src=\"../assets/libra_logo.png\"\r\n      class=\"wallet-img\"\r\n      [routerLink]=\"['']\"\r\n    />\r\n    <span> &nbsp; </span>\r\n    <span noWrap=\"false\"> {{ \"Libra Wallet\" | translate }}</span>\r\n    <span class=\"example-spacer\"></span>\r\n    \r\n    <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\r\n      <mat-icon>more_vert</mat-icon>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\">\r\n      <button *ngIf=\"!isHWWalletConnected\" mat-menu-item (click)=\"openHwWallet()\">\r\n        <mat-icon aria-label=\"Wallet hardware\">hardware</mat-icon>\r\n        <span>{{ \"Hardware Wallet\" | translate }}</span>\r\n      </button>\r\n      <button *ngIf=\"isHWWalletConnected\" mat-menu-item (click)=\"disconnectHwWallet()\">\r\n        <mat-icon aria-label=\"Wallet hardware\">hardware</mat-icon>\r\n        <span>{{ \"Hardware Wallet\" | translate }}</span>\r\n      </button>\r\n  \r\n      <button mat-menu-item (click)=\"switchLang()\">\r\n        <mat-icon aria-label=\"Switch language icon\">language</mat-icon>\r\n        <span>{{ \"Language\" | translate }}</span>\r\n      </button>\r\n      <button mat-menu-item (click)=\"logout()\">\r\n        <mat-icon aria-label=\"Logout icon\">power_settings_new</mat-icon>\r\n        <span>{{ \"Logout\" | translate }}</span>\r\n      </button>\r\n    </mat-menu>\r\n    \r\n  </mat-toolbar>\r\n  <perfect-scrollbar\r\n    *ngIf=\"type === 'component'\"\r\n    class=\"scroll-container\"\r\n    fxFlex=\"auto\"\r\n    [config]=\"config\"\r\n    [scrollIndicators]=\"true\"\r\n    (psXReachEnd)=\"onScrollEvent($event)\"\r\n    (psYReachEnd)=\"onScrollEvent($event)\"\r\n    (psXReachStart)=\"onScrollEvent($event)\"\r\n    (psYReachStart)=\"onScrollEvent($event)\"\r\n  >\r\n    <router-outlet class=\"Site-content\"></router-outlet>\r\n  </perfect-scrollbar>\r\n  <mat-bottom-bar\r\n    fxFlexOffset=\"auto\"\r\n    class=\"footer\"\r\n    [inset]=\"insetToolbar\"\r\n    [fabPosition]=\"fabLocation\"\r\n    [color]=\"color\"\r\n    [flat]=\"flat\"\r\n  >\r\n    <ng-template #mat_right_side_actions>\r\n      <button mat-icon-button>\r\n        <mat-icon [routerLink]=\"['receive']\">money</mat-icon>\r\n      </button>\r\n      <button mat-icon-button>\r\n        <mat-icon [routerLink]=\"['history']\">history</mat-icon>\r\n      </button>\r\n    </ng-template>\r\n    <ng-template #mat_left_side_actions>\r\n      <button mat-icon-button>\r\n        <mat-icon [routerLink]=\"['balance']\">account_balance_wallet</mat-icon>\r\n      </button>\r\n      <button mat-icon-button>\r\n        <mat-icon [routerLink]=\"['send']\">send</mat-icon>\r\n      </button>\r\n    </ng-template>\r\n    <ng-template #mat_fab_area>\r\n      <button mat-fab (click)=\"scan()\">\r\n        <mat-icon>control_camera</mat-icon>\r\n      </button>\r\n    </ng-template>\r\n  </mat-bottom-bar>\r\n</div>\r\n\r\n<ng-template #showLogin>\r\n  <div fxLayout=\"column\" fxFlexFill *ngIf=\"doNotShow\">\r\n    <mat-card class=\"Site-content example-card\">\r\n      <mat-card-header>\r\n        <mat-card-title>{{ \"Sign In\" | translate }}</mat-card-title>\r\n      </mat-card-header>\r\n      <mat-card-content>\r\n        <form [formGroup]=\"loginForm\">\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                placeholder=\"{{ 'email' | translate }}\"\r\n                formControlName=\"email\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                type=\"password\"\r\n                placeholder=\"{{ 'Password' | translate }}\"\r\n                formControlName=\"password\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n        </form>\r\n        <mat-progress-spinner\r\n          style=\"position:fixed;top:50%;left:40%\"\r\n          *ngIf=\"isLoading\"\r\n          color=\"primary\"\r\n          mode=\"indeterminate\"\r\n        >\r\n        </mat-progress-spinner>\r\n      </mat-card-content>\r\n      <mat-card-actions>\r\n        <div fxLayout=\"column\">\r\n          <button\r\n            mat-raised-button\r\n            class=\"login-button\"\r\n            color=\"primary\"\r\n            (click)=\"loginWithEmail()\"\r\n            [disabled]=\"!loginForm.valid\"\r\n          >\r\n            &nbsp;{{ \"Login\" | translate }}\r\n          </button>\r\n          <br>\r\n          <button\r\n            mat-raised-button\r\n            color=\"accent\"\r\n            (click)=\"openAccount()\"\r\n          >\r\n            <mat-icon aria-label=\"Create Account icon\"\r\n              >account_balance_wallet</mat-icon\r\n            >\r\n            <span> Create Account</span>\r\n          </button>\r\n        </div>\r\n      </mat-card-actions>\r\n    </mat-card>\r\n  </div>\r\n\r\n  <div fxLayout=\"row\" fxFlexFill *ngIf=\"doNotShowRegister\">\r\n    <mat-card class=\"Site-content example-card\">\r\n      <mat-card-header>\r\n        <mat-card-title>{{ \"Create Libra Account\" | translate }}</mat-card-title>\r\n      </mat-card-header>\r\n      <mat-card-content>\r\n        <form [formGroup]=\"registerForm\">\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                placeholder=\"{{ 'email' | translate }}\"\r\n                formControlName=\"email\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                type=\"password\"\r\n                placeholder=\"{{ 'Password' | translate }}\"\r\n                formControlName=\"password\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                placeholder=\"{{ 'First Name' | translate }}\"\r\n                formControlName=\"firstName\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input\r\n                matInput\r\n                placeholder=\"{{ 'Last Name' | translate }}\"\r\n                formControlName=\"lastName\"\r\n              />\r\n            </mat-form-field>\r\n          </div>\r\n        </form>\r\n        <mat-progress-spinner\r\n          style=\"position:fixed;top:50%;left:40%\"\r\n          *ngIf=\"isLoading\"\r\n          color=\"primary\"\r\n          mode=\"indeterminate\"\r\n        >\r\n        </mat-progress-spinner>\r\n      </mat-card-content>\r\n      <mat-card-actions class=\"card-actions\">\r\n        <div fxLayout=\"column\">\r\n          <button\r\n            mat-raised-button\r\n            class=\"create-button\"\r\n            color=\"primary\"\r\n            (click)=\"registerWithEmail()\"\r\n            [disabled]=\"!registerForm.valid\"\r\n          >\r\n            &nbsp;{{ \"Create\" | translate }}\r\n          </button>\r\n          <br>\r\n          <button\r\n            mat-raised-button\r\n            class=\"create-button\"\r\n            color=\"accent\"\r\n            (click)=\"cancelCreate()\"\r\n          >\r\n            &nbsp;{{ \"Cancel\" | translate }}\r\n          </button>\r\n        </div>\r\n      </mat-card-actions>\r\n    </mat-card>\r\n  </div>\r\n</ng-template>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bottom-bar/bottom-bar.component.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/bottom-bar/bottom-bar.component.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsBottomBarBottomBarComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<svg id=\"appbar\" width=\"0\" height=\"0\" viewBox=\"0 0 80 36\" xmlns=\"http://www.w3.org/2000/svg\">\r\n    <clipPath id=\"appbar__cutout\">\r\n      <path d=\"M36 36 C 56 36 72 20 72.5 0 V 112 H 0 V 0 C 0 20 16 36 36 36 Z\" />\r\n    </clipPath>\r\n  </svg>\r\n    \r\n    <div class=\"appbar appbar-bottom\" \r\n      [class.appbar-bottom-no-fab]=\"noFab\"\r\n      [class.appbar-bottom-center]=\"normalCenterFab\"\r\n      [class.appbar-bottom-center-inset]=\"insetCenterFab\"\r\n      [class.appbar-bottom-right-end] = \"normalRightEndFab\"\r\n      [class.appbar-bottom-right-end-inset] = \"insetRightEndFab\"\r\n      [class.appbar-bottom-flat] = \"flat\"\r\n      >\r\n      <ng-container *ngIf=\"fabPosition !== 'NONE'\">\r\n        <div class=\"appbar-bottom-fab\"\r\n            [class.appbar-bottom-fab-right-end]=\"rightEndFab\"\r\n            [class.appbar-bottom-fab-left-end]=\"leftEndFab\">\r\n              <ng-container *ngIf=\"fabArea; then fabArea;\"></ng-container>\r\n        </div>\r\n      </ng-container>\r\n      \r\n      <div class=\"appbar-bottom-normal\" \r\n          [class.appbar-bottom-primary]=\"color === 'primary'\"\r\n          [class.appbar-bottom-accent]=\"color === 'accent'\"\r\n          [class.appbar-bottom-warn]=\"color === 'warn'\">\r\n          <div class=\"appbar-bottom-actions\" *ngIf=\"leftSideActions\">\r\n            <ng-container *ngIf=\"leftSideActions; then leftSideActions;\"></ng-container>\r\n          </div>\r\n      </div>\r\n      \r\n      <div class=\"appbar-bottom-cutout\" \r\n        [class.appbar-bottom-primary]=\"color === 'primary'\"\r\n        [class.appbar-bottom-accent]=\"color === 'accent'\"\r\n        [class.appbar-bottom-warn]=\"color === 'warn'\" \r\n        *ngIf=\"inset && !noFab\">\r\n      \r\n      </div>\r\n    \r\n      <div class=\"appbar-bottom-normal flex-right\" \r\n          [class.appbar-bottom-primary]=\"color === 'primary'\"\r\n          [class.appbar-bottom-accent]=\"color === 'accent'\"\r\n          [class.appbar-bottom-warn]=\"color === 'warn'\"\r\n          *ngIf=\"isCenteredOrInsetRightBar\" >\r\n          <div class=\"appbar-bottom-actions\" *ngIf=\"rightSideActions && !rightEndFab\">\r\n            <ng-container *ngIf=\"rightSideActions; then rightSideActions;\"></ng-container>\r\n          </div>\r\n      </div>\r\n    </div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/main/main.component.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/main/main.component.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsMainMainComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-grid-list cols=\"1\" rows=\"5\" rowHeight=\"100px\">\r\n  <mat-grid-tile>\r\n    <img class=\"wallet-img\" src=\"../../../assets/wallet-logo.png\" />\r\n    <span>&nbsp;&nbsp;{{ \"Hello !\" | translate }} {{firstName}}</span>\r\n  </mat-grid-tile>\r\n  <mat-grid-tile>\r\n    {{ \"Wallet\" | translate }}: {{ libraAccount | slice: 0:6 }}...{{\r\n      libraAccount | slice: 16:23\r\n    }}<button\r\n      mat-icon-button\r\n      aria-label=\"copy clipboard\"\r\n      (click)=\"copyAccount()\"\r\n    >\r\n      <mat-icon>file_copy</mat-icon>\r\n    </button>\r\n  </mat-grid-tile>\r\n  <mat-grid-tile>\r\n    <img class=\"logo-img\" src=\"../../../assets/libra-logo.jpg\" /> {{ amount }}\r\n    {{ \"Libra\" | translate }}\r\n  </mat-grid-tile>\r\n  <mat-grid-tile>\r\n    <mat-chip-list aria-label=\"Network Status\">\r\n      <mat-chip color=\"accent\" selected>{{\r\n        \"Test Network\" | translate\r\n      }}</mat-chip>\r\n      <button\r\n        mat-icon-button\r\n        aria-label=\"refresh amount\"\r\n        (click)=\"refreshAmount()\"\r\n      >\r\n        <mat-icon>refresh</mat-icon>\r\n      </button>\r\n    </mat-chip-list>\r\n  </mat-grid-tile>\r\n</mat-grid-list>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/receive/receive.component.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/receive/receive.component.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsReceiveReceiveComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"example-card\">\r\n  <mat-card-content>\r\n    <div align=\"center\">\r\n      <ngx-qrcode [qrc-value]=\"value\"></ngx-qrcode>\r\n      <p class=\"wrap\">{{ value }}</p>\r\n    </div>\r\n  </mat-card-content>\r\n  <mat-card-actions align=\"center\">\r\n    <button mat-raised-button (click)=\"copyToClipboard()\">\r\n      {{ \"Copy to Clipboard\" | translate }}\r\n    </button>\r\n    <button mat-raised-button (click)=\"rotateKey()\" disabled=\"true\">\r\n      {{ \"Rotate Key\" | translate }}\r\n    </button>\r\n  </mat-card-actions>\r\n</mat-card>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/scan/scan.component.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/scan/scan.component.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsScanScanComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<zxing-scanner\r\n  [enable]=\"scannerEnabled\"\r\n  [torch]=\"torchEnabled\"\r\n  [(device)]=\"currentDevice\"\r\n  (scanSuccess)=\"onCodeResult($event)\"\r\n  [formats]=\"formatsEnabled\"\r\n  [tryHarder]=\"tryHarder\"\r\n  (permissionResponse)=\"onHasPermission($event)\"\r\n  (camerasFound)=\"onCamerasFound($event)\"\r\n  (torchCompatible)=\"onTorchCompatible($event)\"\r\n></zxing-scanner>\r\n<div align=\"center\">\r\n  <h2>{{ \"Scan your Libra Wallet QR Code here\" | translate }}.</h2>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/confirm.component.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/confirm.component.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsSendConfirmComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div style=\"width:100%;height:0;padding-bottom:76%;position:relative;\">\r\n  <iframe\r\n    src=\"https://giphy.com/embed/ESt8At0PXpmj6\"\r\n    width=\"100%\"\r\n    height=\"100%\"\r\n    style=\"position:absolute\"\r\n    frameBorder=\"0\"\r\n    class=\"giphy-embed\"\r\n    allowFullScreen\r\n  ></iframe>\r\n</div>\r\n<p><a href=\"https://giphy.com/gifs/money-jetsons-cartoon-ESt8At0PXpmj6\"></a></p>\r\n<br />\r\n<div align=\"center\">\r\n  <h2>\r\n    <teximate [text]=\"message\" [enter]=\"enterAnimation\"></teximate>\r\n  </h2>\r\n</div>\r\n<br />\r\n<div align=\"center\">\r\n  <button mat-raised-button color=\"primary\" (click)=\"navigateToHistory()\">\r\n    {{ \"Go to transaction history\" | translate }}\r\n  </button>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/send.component.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/send.component.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsSendSendComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card>\r\n  <div class=\"example-container\">\r\n    <form class=\"example-container\" [formGroup]=\"sendForm\">\r\n      <mat-form-field>\r\n        <input\r\n          matInput\r\n          placeholder=\"{{ 'Destination Address' | translate }}\"\r\n          formControlName=\"destAccount\"\r\n          required\r\n        />\r\n      </mat-form-field>\r\n      <mat-form-field>\r\n        <input\r\n          matInput\r\n          placeholder=\"{{ 'Amount' | translate }}\"\r\n          formControlName=\"amount\"\r\n          required\r\n        />\r\n      </mat-form-field>\r\n      <div *ngIf=\"fc.amount.errors && (fc.amount.touched)\">\r\n        <div *ngIf=\"fc.amount.errors.pattern\">\r\n          <mat-error [hidden]=\"!fc.amount.errors.pattern\">\r\n              Amount must be money.\r\n          </mat-error>\r\n      </div>\r\n\r\n    </div>\r\n      <button\r\n        mat-raised-button\r\n        color=\"primary\"\r\n        (click)=\"onSend()\"\r\n        [disabled]=\"!sendForm.valid\"\r\n      >\r\n        {{ \"Send\" | translate }}\r\n      </button>\r\n    </form>\r\n    <mat-progress-spinner\r\n      style=\"position:fixed;top:50%;left:40%\"\r\n      *ngIf=\"isLoading\"\r\n      color=\"primary\"\r\n      mode=\"indeterminate\"\r\n    >\r\n    </mat-progress-spinner>\r\n  </div>\r\n</mat-card>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/transaction-history/transaction-history.component.html":
  /*!*************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/transaction-history/transaction-history.component.html ***!
    \*************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsTransactionHistoryTransactionHistoryComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card>\r\n  <mat-list role=\"list\">\r\n    <mat-card class=\"example-card\" *ngFor=\"let trx of transactionHistory\">\r\n        <mat-card-header>\r\n                <mat-card-title>\r\n                        <mat-chip-list *ngIf=\"trx.event==='received'\" aria-label=\"received selection\">\r\n                        <mat-chip color=\"primary\" selected>{{ \"Received\" | translate }}</mat-chip></mat-chip-list>\r\n                        <mat-chip-list *ngIf=\"trx.event!=='received'\" aria-label=\"received selection\">\r\n                                <mat-chip color=\"warn\" selected>{{ \"Sent\" | translate }}</mat-chip></mat-chip-list>\r\n                         fromAddress : \r\n                         <button mat-button\r\n                         matTooltip=\"{{trx.fromAddress}}\"\r\n                         aria-label=\"Button that displays a tooltip when focused or hovered over\">\r\n                         ...{{trx.fromAddress | slice:trx.fromAddress.length-6:trx.fromAddress.length}}\r\n                        </button>\r\n                         <br>\r\n                         toAddress : \r\n                         <button mat-button\r\n                         matTooltip=\"{{trx.toAddress}}\"\r\n                         aria-label=\"Button that displays a tooltip when focused or hovered over\">\r\n                         ...{{trx.toAddress | slice:trx.toAddress.length-6:trx.toAddress.length}}</button>\r\n                        </mat-card-title>\r\n                <mat-card-subtitle>{{trx.date | date: 'medium'}}</mat-card-subtitle>\r\n        </mat-card-header>\r\n        <mat-card-content>\r\n                <b>{{trx.amount}} Libra\r\n                </b>\r\n        </mat-card-content>\r\n        <mat-card-actions>\r\n          <a href=\"{{trx.explorerLink}}\" mat-menu-item>Explorer</a>\r\n        </mat-card-actions>\r\n     </mat-card>\r\n    <mat-divider></mat-divider>\r\n  </mat-list>\r\n</mat-card>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/lang-selector.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lang-selector.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLangSelectorHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-nav-list>\r\n<a href=\"#\" mat-list-item (click)=\"selectLang($event,'en')\">\r\n    <span mat-line>EN - English</span>\r\n</a>\r\n<a href=\"#\" mat-list-item (click)=\"selectLang($event,'zh')\">\r\n    <span mat-line>汉语 - Chinese</span>\r\n</a>\r\n</mat-nav-list>";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _components_main_main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./components/main/main.component */
    "./src/app/components/main/main.component.ts");
    /* harmony import */


    var _components_scan_scan_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/scan/scan.component */
    "./src/app/components/scan/scan.component.ts");
    /* harmony import */


    var _components_send_send_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/send/send.component */
    "./src/app/components/send/send.component.ts");
    /* harmony import */


    var _components_send_confirm_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/send/confirm.component */
    "./src/app/components/send/confirm.component.ts");
    /* harmony import */


    var _components_receive_receive_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/receive/receive.component */
    "./src/app/components/receive/receive.component.ts");
    /* harmony import */


    var _components_transaction_history_transaction_history_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/transaction-history/transaction-history.component */
    "./src/app/components/transaction-history/transaction-history.component.ts");

    var routes = [{
      path: "",
      component: _components_main_main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"]
    }, {
      path: "scan",
      component: _components_scan_scan_component__WEBPACK_IMPORTED_MODULE_4__["ScanComponent"]
    }, {
      path: "send",
      component: _components_send_send_component__WEBPACK_IMPORTED_MODULE_5__["SendComponent"]
    }, {
      path: "confirm-send",
      component: _components_send_confirm_component__WEBPACK_IMPORTED_MODULE_6__["ConfirmComponent"]
    }, {
      path: "receive",
      component: _components_receive_receive_component__WEBPACK_IMPORTED_MODULE_7__["ReceiveComponent"]
    }, {
      path: "history",
      component: _components_transaction_history_transaction_history_component__WEBPACK_IMPORTED_MODULE_8__["TransactionHistoryComponent"]
    }, {
      path: "",
      redirectTo: "/",
      pathMatch: "full"
    }, {
      path: "**",
      redirectTo: "/",
      pathMatch: "full"
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host,\n:host main {\n  -webkit-transition: background 170ms ease-in;\n  transition: background 170ms ease-in;\n  width: 100vw;\n  height: 100vh;\n}\n:host.dark-theme,\n:host.dark-theme main {\n  background: #010101;\n  color: #fff;\n}\n.list {\n  padding: 2rem;\n}\nh1,\nh2 {\n  font-size: 1rem;\n}\n.example-icon {\n  padding: 0 14px;\n}\n.example-spacer {\n  -webkit-box-flex: 1;\n          flex: 1 1 500px;\n}\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n.example-full-width {\n  width: 100%;\n}\n.example-card {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  max-width: 100%;\n}\n.example-header-image {\n  background-size: cover;\n}\n.example-margin {\n  margin: 0 10px;\n}\n.transparent .mat-dialog-container {\n  box-shadow: none;\n  background: rgba(0, 0, 0, 0);\n}\nhtml,\nbody {\n  height: 100%;\n}\nbody {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n.Site-content {\n  -webkit-box-flex: 1;\n          flex: 1 0 auto;\n}\n.footer {\n  flex-shrink: 0;\n}\n.login-button {\n  width: 100% !important;\n  min-width: unset !important;\n}\n.gesture__zone {\n  padding: 20px;\n  margin: 10px;\n  border-radius: 3px;\n  height: 500px;\n  text-align: center;\n  overflow: auto;\n}\n.wallet-img {\n  border-radius: 30%;\n  padding: 5px;\n  width: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRTpcXFByb2plY3RzXFxhbmd1bGFyLWxpYnJhLXdhbGxldC9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7O0VBRUUsNENBQUE7RUFBQSxvQ0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FDQUo7QURJSTs7RUFFRSxtQkFBQTtFQUNBLFdBQUE7QUNGTjtBRE9BO0VBQ0UsYUFBQTtBQ0pGO0FET0E7O0VBRUUsZUFBQTtBQ0pGO0FET0E7RUFDRSxlQUFBO0FDSkY7QURNQTtFQUNFLG1CQUFBO1VBQUEsZUFBQTtBQ0hGO0FES0E7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ0ZGO0FESUE7RUFDRSxXQUFBO0FDREY7QURHQTtFQUNFLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsZUFBQTtBQ0FGO0FERUE7RUFDRSxzQkFBQTtBQ0NGO0FEQ0E7RUFDRSxjQUFBO0FDRUY7QURDRTtFQUNFLGdCQUFBO0VBQ0EsNEJBQUE7QUNFSjtBREVBOztFQUVFLFlBQUE7QUNDRjtBRENBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0EsNEJBQUE7RUFBQSw2QkFBQTtVQUFBLHNCQUFBO0FDRUY7QURDQTtFQUNFLG1CQUFBO1VBQUEsY0FBQTtBQ0VGO0FEQ0E7RUFDRSxjQUFBO0FDRUY7QURDQTtFQUNFLHNCQUFBO0VBQ0EsMkJBQUE7QUNFRjtBRENBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNFRjtBRENBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gICYsXHJcbiAgbWFpbiB7XHJcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIDE3MG1zIGVhc2UtaW47XHJcbiAgICB3aWR0aDogMTAwdnc7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gIH1cclxuXHJcbiAgJi5kYXJrLXRoZW1lIHtcclxuICAgICYsXHJcbiAgICBtYWluIHtcclxuICAgICAgYmFja2dyb3VuZDogIzAxMDEwMTtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ubGlzdCB7XHJcbiAgcGFkZGluZzogMnJlbTtcclxufVxyXG5cclxuaDEsXHJcbmgyIHtcclxuICBmb250LXNpemU6IDFyZW07XHJcbn1cclxuXHJcbi5leGFtcGxlLWljb24ge1xyXG4gIHBhZGRpbmc6IDAgMTRweDtcclxufVxyXG4uZXhhbXBsZS1zcGFjZXIge1xyXG4gIGZsZXg6IDEgMSA1MDBweDtcclxufVxyXG4uZXhhbXBsZS1mb3JtIHtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIG1heC13aWR0aDogNTAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmV4YW1wbGUtY2FyZCB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbn1cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcbi5leGFtcGxlLW1hcmdpbiB7XHJcbiAgbWFyZ2luOiAwIDEwcHg7XHJcbn1cclxuLnRyYW5zcGFyZW50IHtcclxuICAubWF0LWRpYWxvZy1jb250YWluZXIge1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMCk7XHJcbiAgfVxyXG59XHJcblxyXG5odG1sLFxyXG5ib2R5IHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuYm9keSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4uU2l0ZS1jb250ZW50IHtcclxuICBmbGV4OiAxIDAgYXV0bztcclxufVxyXG5cclxuLmZvb3RlciB7XHJcbiAgZmxleC1zaHJpbms6IDA7XHJcbn1cclxuXHJcbi5sb2dpbi1idXR0b24ge1xyXG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgbWluLXdpZHRoOiB1bnNldCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZ2VzdHVyZV9fem9uZSB7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBtYXJnaW46IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gIGhlaWdodDogNTAwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4ud2FsbGV0LWltZyB7XHJcbiAgYm9yZGVyLXJhZGl1czogMzAlO1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICB3aWR0aDogNTBweDtcclxufVxyXG4iLCI6aG9zdCxcbjpob3N0IG1haW4ge1xuICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIDE3MG1zIGVhc2UtaW47XG4gIHdpZHRoOiAxMDB2dztcbiAgaGVpZ2h0OiAxMDB2aDtcbn1cbjpob3N0LmRhcmstdGhlbWUsXG46aG9zdC5kYXJrLXRoZW1lIG1haW4ge1xuICBiYWNrZ3JvdW5kOiAjMDEwMTAxO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLmxpc3Qge1xuICBwYWRkaW5nOiAycmVtO1xufVxuXG5oMSxcbmgyIHtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4uZXhhbXBsZS1pY29uIHtcbiAgcGFkZGluZzogMCAxNHB4O1xufVxuXG4uZXhhbXBsZS1zcGFjZXIge1xuICBmbGV4OiAxIDEgNTAwcHg7XG59XG5cbi5leGFtcGxlLWZvcm0ge1xuICBtaW4td2lkdGg6IDE1MHB4O1xuICBtYXgtd2lkdGg6IDUwMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZXhhbXBsZS1jYXJkIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cblxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmV4YW1wbGUtbWFyZ2luIHtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi50cmFuc3BhcmVudCAubWF0LWRpYWxvZy1jb250YWluZXIge1xuICBib3gtc2hhZG93OiBub25lO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApO1xufVxuXG5odG1sLFxuYm9keSB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuYm9keSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5TaXRlLWNvbnRlbnQge1xuICBmbGV4OiAxIDAgYXV0bztcbn1cblxuLmZvb3RlciB7XG4gIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4ubG9naW4tYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgbWluLXdpZHRoOiB1bnNldCAhaW1wb3J0YW50O1xufVxuXG4uZ2VzdHVyZV9fem9uZSB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBoZWlnaHQ6IDUwMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG4ud2FsbGV0LWltZyB7XG4gIGJvcmRlci1yYWRpdXM6IDMwJTtcbiAgcGFkZGluZzogNXB4O1xuICB3aWR0aDogNTBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: BottomSheetLanguageSheet, AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BottomSheetLanguageSheet", function () {
      return BottomSheetLanguageSheet;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _components_bottom_bar_bottom_bar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./components/bottom-bar/bottom-bar.component */
    "./src/app/components/bottom-bar/bottom-bar.component.ts");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _services_ble_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./services/ble.service */
    "./src/app/services/ble.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var BottomSheetLanguageSheet =
    /*#__PURE__*/
    function () {
      function BottomSheetLanguageSheet(bottomSheetRef, translate) {
        _classCallCheck(this, BottomSheetLanguageSheet);

        this.bottomSheetRef = bottomSheetRef;
        this.translate = translate;
      }

      _createClass(BottomSheetLanguageSheet, [{
        key: "selectLang",
        value: function selectLang(event, indicator) {
          if (indicator === "en") {
            this.translate.setDefaultLang("en");
          }

          if (indicator == "zh") {
            this.translate.setDefaultLang("zh");
          }

          this.bottomSheetRef.dismiss();
          event.preventDefault();
        }
      }]);

      return BottomSheetLanguageSheet;
    }();

    BottomSheetLanguageSheet.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatBottomSheetRef"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateService"]
      }];
    };

    BottomSheetLanguageSheet = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "filter-language",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./lang-selector.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/lang-selector.html")).default
    })], BottomSheetLanguageSheet);

    var AppComponent =
    /*#__PURE__*/
    function () {
      function AppComponent(router, fb, afAuth, authService, snackBar, translate, walletSvc, bleSvc, bottomSheet) {
        _classCallCheck(this, AppComponent);

        this.router = router;
        this.fb = fb;
        this.afAuth = afAuth;
        this.authService = authService;
        this.snackBar = snackBar;
        this.translate = translate;
        this.walletSvc = walletSvc;
        this.bleSvc = bleSvc;
        this.bottomSheet = bottomSheet;
        this.title = "angular-libra-wallet";
        this.insetToolbar = true;
        this.fabLocation = _components_bottom_bar_bottom_bar_component__WEBPACK_IMPORTED_MODULE_2__["Fablocation"].CENTER;
        this.color = "primary";
        this.flat = false;
        this.type = 'component';
        this.isHWWalletConnected = false;
        this.disabled = false;
        this.config = {};
        this.PASSWORD_PATTERN = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{12,}$/;
        this.isLoading = false;
        this.accountAlreadyActivated = false;
        this.doNotShow = true;
        this.doNotShowRegister = false;
        this.loginForm = fb.group({
          email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].email]],
          password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern(this.PASSWORD_PATTERN)]]
        });
        this.registerForm = fb.group({
          email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].email]],
          password: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern(this.PASSWORD_PATTERN)]],
          firstName: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(3)]],
          lastName: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(3)]]
        });
        translate.setDefaultLang('en');
      }

      _createClass(AppComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          if (typeof this.authService.getEmail() !== 'undefined') {
            this.walletSvc.getWalletByEmail().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["map"])(function (changes) {
              return changes.map(function (c) {
                return Object.assign({
                  key: c.payload.key
                }, c.payload.val());
              });
            })).subscribe(function (wallet) {
              console.log(wallet[0].address);

              if (!wallet[0].address && !wallet[0].mnemonic) {
                console.log("already activated");
                _this.accountAlreadyActivated = true;
              }
            });
          }
        }
      }, {
        key: "switchLang",
        value: function switchLang() {
          this.bottomSheet.open(BottomSheetLanguageSheet);
        }
      }, {
        key: "onScrollEvent",
        value: function onScrollEvent(event) {}
      }, {
        key: "scan",
        value: function scan() {
          this.router.navigate(["/scan"]);
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (typeof this.loginWithEmailSub !== "undefined") {
            this.loginWithEmailSub.unsubscribe();
          }
        }
      }, {
        key: "loginWithEmail",
        value: function loginWithEmail() {
          var _this2 = this;

          var formValue = this.loginForm.value;
          this.isLoading = true;

          try {
            this.loginWithEmailSub = this.authService.loginWithEmail(formValue.email, formValue.password).subscribe(function (result) {
              _this2.authService.setFirebaseTokenToLocalstorage();

              setTimeout(function () {
                this.isLoading = false;
              }.bind(_this2), 5000);
            }, function (error) {
              _this2.isLoading = false; // error path
            });
          } catch (e) {
            this.isLoading = false;
          }
        }
      }, {
        key: "registerWithEmail",
        value: function registerWithEmail() {
          var _this3 = this;

          this.walletSvc.createWallet().then(function (result) {
            var formValue = _this3.registerForm.value;
            console.log(result);
            var w = {
              email: String(formValue.email),
              mnemonic: result.mnemonic,
              address: result.address,
              firstName: formValue.firstName,
              lastName: formValue.lastName
            };

            _this3.authService.registerWithEmail(formValue.email, formValue.password).subscribe(function (result) {
              setTimeout(function () {}.bind(_this3), 4000);
              console.log(result);

              _this3.walletSvc.saveWallet(w).subscribe(function (result) {
                console.log(">>>> " + result);

                var snackBarRef = _this3.snackBar.open("Wallet created", "done", {
                  duration: 3000
                });
              });
            });
          });
        }
      }, {
        key: "logout",
        value: function logout() {
          var _this4 = this;

          this.isLoading = false;
          this.afAuth.auth.signOut().then(function (result) {
            console.log(result);

            _this4.authService.destroyToken();

            _this4.doNotShow = true;
            _this4.doNotShowRegister = false;
          });
        }
      }, {
        key: "openAccount",
        value: function openAccount() {
          this.doNotShow = false;
          this.doNotShowRegister = true;
        }
      }, {
        key: "cancelCreate",
        value: function cancelCreate() {
          this.doNotShow = true;
          this.doNotShowRegister = false;
        }
      }, {
        key: "openHwWallet",
        value: function openHwWallet() {
          var serviceUuid = 'c03e7090-7ce0-46f0-98dd-a2aba8367741';
          var characteristicUuid = '26e2b12b-85f0-4f3f-9fdd-91d114270e6e';
          var device = null;
          var resultfromHW = '';

          try {} catch (error) {
            console.log('Argh! ' + error);
          }
        }
      }, {
        key: "disconnectHwWallet",
        value: function disconnectHwWallet() {
          console.log("disconnect Hw wallet");
          this.bleSvc.disconnectDevice();
          this.deviceSubscription.unsubscribe();
          this.valuesSubscription.unsubscribe();
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
      }, {
        type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__["AngularFireAuth"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSnackBar"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateService"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_9__["WalletService"]
      }, {
        type: _services_ble_service__WEBPACK_IMPORTED_MODULE_11__["BleService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatBottomSheet"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__["PerfectScrollbarComponent"], {
      static: false
    })], AppComponent.prototype, "componentRef", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__["PerfectScrollbarDirective"], {
      static: false
    })], AppComponent.prototype, "directiveRef", void 0);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-root",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: MyHammerConfig, AppModule, HttpLoaderFactory */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHammerConfig", function () {
      return MyHammerConfig;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function () {
      return HttpLoaderFactory;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/fesm2015/animations.js");
    /* harmony import */


    var _components_scan_scan_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/scan/scan.component */
    "./src/app/components/scan/scan.component.ts");
    /* harmony import */


    var _components_send_send_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/send/send.component */
    "./src/app/components/send/send.component.ts");
    /* harmony import */


    var _components_bottom_bar_bottom_bar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/bottom-bar/bottom-bar.component */
    "./src/app/components/bottom-bar/bottom-bar.component.ts");
    /* harmony import */


    var _components_main_main_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./components/main/main.component */
    "./src/app/components/main/main.component.ts");
    /* harmony import */


    var _components_transaction_history_transaction_history_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./components/transaction-history/transaction-history.component */
    "./src/app/components/transaction-history/transaction-history.component.ts");
    /* harmony import */


    var _components_receive_receive_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./components/receive/receive.component */
    "./src/app/components/receive/receive.component.ts");
    /* harmony import */


    var _components_send_confirm_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./components/send/confirm.component */
    "./src/app/components/send/confirm.component.ts");
    /* harmony import */


    var _angular_service_worker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/service-worker */
    "./node_modules/@angular/service-worker/fesm2015/service-worker.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _material_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./material.module */
    "./src/app/material.module.ts");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var hammerjs__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! hammerjs */
    "./node_modules/hammerjs/hammer.js");
    /* harmony import */


    var hammerjs__WEBPACK_IMPORTED_MODULE_19___default =
    /*#__PURE__*/
    __webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_19__);
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _zxing_ngx_scanner__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! @zxing/ngx-scanner */
    "./node_modules/@zxing/ngx-scanner/fesm2015/zxing-ngx-scanner.js");
    /* harmony import */


    var ngx_qrcode3__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ngx-qrcode3 */
    "./node_modules/ngx-qrcode3/index.js");
    /* harmony import */


    var ngx_clipboard__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ngx-clipboard */
    "./node_modules/ngx-clipboard/fesm2015/ngx-clipboard.js");
    /* harmony import */


    var ngx_teximate__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! ngx-teximate */
    "./node_modules/ngx-teximate/fesm2015/ngx-teximate.js");
    /* harmony import */


    var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! @angular/fire/firestore */
    "./node_modules/@angular/fire/firestore/es2015/index.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var _angular_fire_messaging__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
    /*! @angular/fire/messaging */
    "./node_modules/@angular/fire/messaging/es2015/index.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
    /*! @ngx-translate/http-loader */
    "./node_modules/@ngx-translate/http-loader/fesm2015/ngx-translate-http-loader.js");
    /* harmony import */


    var _manekinekko_angular_web_bluetooth__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
    /*! @manekinekko/angular-web-bluetooth */
    "./node_modules/@manekinekko/angular-web-bluetooth/fesm2015/manekinekko-angular-web-bluetooth.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
    /*! ./services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _services_auth_interceptor__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
    /*! ./services/auth.interceptor */
    "./src/app/services/auth.interceptor.ts");
    /* harmony import */


    var _shared_localized_date_pipe__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
    /*! ./shared/localized-date.pipe */
    "./src/app/shared/localized-date.pipe.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_locales_en__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
    /*! @angular/common/locales/en */
    "./node_modules/@angular/common/locales/en.js");
    /* harmony import */


    var _angular_common_locales_en__WEBPACK_IMPORTED_MODULE_38___default =
    /*#__PURE__*/
    __webpack_require__.n(_angular_common_locales_en__WEBPACK_IMPORTED_MODULE_38__);
    /* harmony import */


    var _angular_common_locales_zh__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
    /*! @angular/common/locales/zh */
    "./node_modules/@angular/common/locales/zh.js");
    /* harmony import */


    var _angular_common_locales_zh__WEBPACK_IMPORTED_MODULE_39___default =
    /*#__PURE__*/
    __webpack_require__.n(_angular_common_locales_zh__WEBPACK_IMPORTED_MODULE_39__);
    /* harmony import */


    var _angular_common_locales_extra_he__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
    /*! @angular/common/locales/extra/he */
    "./node_modules/@angular/common/locales/extra/he.js");
    /* harmony import */


    var _angular_common_locales_extra_he__WEBPACK_IMPORTED_MODULE_40___default =
    /*#__PURE__*/
    __webpack_require__.n(_angular_common_locales_extra_he__WEBPACK_IMPORTED_MODULE_40__);

    var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
      suppressScrollX: true
    };

    var MyHammerConfig =
    /*#__PURE__*/
    function (_angular_platform_bro) {
      _inherits(MyHammerConfig, _angular_platform_bro);

      function MyHammerConfig() {
        var _this5;

        _classCallCheck(this, MyHammerConfig);

        _this5 = _possibleConstructorReturn(this, _getPrototypeOf(MyHammerConfig).apply(this, arguments));
        _this5.overrides = {
          swipe: {
            direction: hammerjs__WEBPACK_IMPORTED_MODULE_19__["DIRECTION_ALL"]
          }
        };
        return _this5;
      }

      return MyHammerConfig;
    }(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["HammerGestureConfig"]);

    Object(_angular_common__WEBPACK_IMPORTED_MODULE_37__["registerLocaleData"])(_angular_common_locales_en__WEBPACK_IMPORTED_MODULE_38___default.a, 'en');
    Object(_angular_common__WEBPACK_IMPORTED_MODULE_37__["registerLocaleData"])(_angular_common_locales_zh__WEBPACK_IMPORTED_MODULE_39___default.a, 'zh', _angular_common_locales_extra_he__WEBPACK_IMPORTED_MODULE_40___default.a);

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      entryComponents: [_app_component__WEBPACK_IMPORTED_MODULE_4__["BottomSheetLanguageSheet"]],
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"], _components_scan_scan_component__WEBPACK_IMPORTED_MODULE_6__["ScanComponent"], _components_send_send_component__WEBPACK_IMPORTED_MODULE_7__["SendComponent"], _components_transaction_history_transaction_history_component__WEBPACK_IMPORTED_MODULE_10__["TransactionHistoryComponent"], _components_receive_receive_component__WEBPACK_IMPORTED_MODULE_11__["ReceiveComponent"], _components_bottom_bar_bottom_bar_component__WEBPACK_IMPORTED_MODULE_8__["BottomBarComponent"], _components_main_main_component__WEBPACK_IMPORTED_MODULE_9__["MainComponent"], _components_send_confirm_component__WEBPACK_IMPORTED_MODULE_12__["ConfirmComponent"], _app_component__WEBPACK_IMPORTED_MODULE_4__["BottomSheetLanguageSheet"], _shared_localized_date_pipe__WEBPACK_IMPORTED_MODULE_36__["LocalizedDatePipe"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"], _angular_service_worker__WEBPACK_IMPORTED_MODULE_13__["ServiceWorkerModule"].register('ngsw-worker.js', {
        enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].production
      }), _material_module__WEBPACK_IMPORTED_MODULE_15__["MaterialModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_16__["FlexLayoutModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_18__["PerfectScrollbarModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClientModule"], ngx_qrcode3__WEBPACK_IMPORTED_MODULE_22__["NgxQRCodeModule"], _zxing_ngx_scanner__WEBPACK_IMPORTED_MODULE_21__["ZXingScannerModule"], ngx_clipboard__WEBPACK_IMPORTED_MODULE_23__["ClipboardModule"], ngx_teximate__WEBPACK_IMPORTED_MODULE_24__["TeximateModule"], _angular_fire__WEBPACK_IMPORTED_MODULE_28__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].firebase), _angular_fire_database__WEBPACK_IMPORTED_MODULE_26__["AngularFireDatabaseModule"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_29__["AngularFireStorageModule"], _angular_fire_messaging__WEBPACK_IMPORTED_MODULE_30__["AngularFireMessagingModule"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_27__["AngularFireAuthModule"], _manekinekko_angular_web_bluetooth__WEBPACK_IMPORTED_MODULE_33__["WebBluetoothModule"].forRoot({
        enableTracing: true
      }), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__["TranslateModule"].forRoot({
        loader: {
          provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__["TranslateLoader"],
          useFactory: HttpLoaderFactory,
          deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClient"]]
        }
      })],
      providers: [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_25__["AngularFirestore"], _services_auth_service__WEBPACK_IMPORTED_MODULE_34__["AuthService"], {
        provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_18__["PERFECT_SCROLLBAR_CONFIG"],
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
      }, {
        provide: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["HAMMER_GESTURE_CONFIG"],
        useClass: MyHammerConfig
      }, {
        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HTTP_INTERCEPTORS"],
        useClass: _services_auth_interceptor__WEBPACK_IMPORTED_MODULE_35__["AuthInterceptor"],
        multi: true
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule); // required for AOT compilation

    function HttpLoaderFactory(http) {
      return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_32__["TranslateHttpLoader"](http);
    }
    /***/

  },

  /***/
  "./src/app/components/bottom-bar/bottom-bar.component.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/components/bottom-bar/bottom-bar.component.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsBottomBarBottomBarComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host {\n  position: relative;\n  z-index: 1;\n}\n\n.flex-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib3R0b20tYmFyL0U6XFxQcm9qZWN0c1xcYW5ndWxhci1saWJyYS13YWxsZXQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGJvdHRvbS1iYXJcXGJvdHRvbS1iYXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvYm90dG9tLWJhci9ib3R0b20tYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0FDQ0Y7O0FERUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0FDQ0EiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2JvdHRvbS1iYXIvYm90dG9tLWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogMTtcclxufSBcclxuXHJcbi5mbGV4LXJpZ2h0IHtcclxuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufSIsIjpob3N0IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xufVxuXG4uZmxleC1yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/components/bottom-bar/bottom-bar.component.ts":
  /*!***************************************************************!*\
    !*** ./src/app/components/bottom-bar/bottom-bar.component.ts ***!
    \***************************************************************/

  /*! exports provided: Fablocation, AppbarClassNames, BottomBarComponent */

  /***/
  function srcAppComponentsBottomBarBottomBarComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Fablocation", function () {
      return Fablocation;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppbarClassNames", function () {
      return AppbarClassNames;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BottomBarComponent", function () {
      return BottomBarComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var Fablocation;

    (function (Fablocation) {
      Fablocation["NONE"] = "NONE";
      Fablocation["CENTER"] = "CENTER";
      Fablocation["RIGHT_END"] = "RIGHT-END";
      Fablocation["LEFT_END"] = "LEFT-END";
    })(Fablocation || (Fablocation = {}));

    var AppbarClassNames;

    (function (AppbarClassNames) {
      AppbarClassNames["NO_FAB"] = "appbar-bottom-no-fab";
      AppbarClassNames["CENTER_FAB"] = "appbar-bottom-center";
      AppbarClassNames["CENTER_FAB_INSET"] = "appbar-bottom-center-cut";
      AppbarClassNames["LEFT_END_FAB"] = "appbar-bottom-left-end";
      AppbarClassNames["LEFT_END_FAB_INSET"] = "appbar-bottom-left-end-cut";
      AppbarClassNames["RIGHT_END_FAB"] = "appbar-bottom-right-end";
      AppbarClassNames["RIGHT_END_FAB_INSET"] = "appbar-bottom-left-end-cut";
    })(AppbarClassNames || (AppbarClassNames = {}));

    var BottomBarComponent =
    /*#__PURE__*/
    function () {
      function BottomBarComponent() {
        _classCallCheck(this, BottomBarComponent);

        this.inset = false;
        this.fabPosition = Fablocation.NONE;
        this.color = 'primary';
        this.flat = false;
      }

      _createClass(BottomBarComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "normalCenterFab",
        get: function get() {
          return !this.inset && this.fabPosition === Fablocation.CENTER;
        }
      }, {
        key: "normalRightEndFab",
        get: function get() {
          return !this.inset && this.fabPosition === Fablocation.RIGHT_END;
        }
      }, {
        key: "insetCenterFab",
        get: function get() {
          return this.inset && this.fabPosition === Fablocation.CENTER;
        }
      }, {
        key: "insetRightEndFab",
        get: function get() {
          return this.inset && this.fabPosition === Fablocation.RIGHT_END;
        }
      }, {
        key: "noFab",
        get: function get() {
          return this.fabPosition === Fablocation.NONE || !this.fabArea;
        }
      }, {
        key: "rightEndFab",
        get: function get() {
          return this.fabPosition === Fablocation.RIGHT_END;
        }
      }, {
        key: "leftEndFab",
        get: function get() {
          return this.fabPosition === Fablocation.LEFT_END;
        }
      }, {
        key: "centerFab",
        get: function get() {
          return this.fabPosition === Fablocation.CENTER;
        }
      }, {
        key: "isCenteredOrInsetRightBar",
        get: function get() {
          if (this.centerFab) {
            return true;
          } else if (this.insetRightEndFab) {
            return true;
          } else if (this.noFab) {
            return true;
          } else {
            return false;
          }
        }
      }]);

      return BottomBarComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], BottomBarComponent.prototype, "inset", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], BottomBarComponent.prototype, "fabPosition", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], BottomBarComponent.prototype, "color", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], BottomBarComponent.prototype, "flat", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])('mat_inset_cutout', {
      static: false
    })], BottomBarComponent.prototype, "cutOutTemplate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])('mat_left_side_actions', {
      static: false
    })], BottomBarComponent.prototype, "leftSideActions", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])('mat_right_side_actions', {
      static: false
    })], BottomBarComponent.prototype, "rightSideActions", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])('mat_fab_area', {
      static: false
    })], BottomBarComponent.prototype, "fabArea", void 0);
    BottomBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'mat-bottom-bar',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./bottom-bar.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bottom-bar/bottom-bar.component.html")).default,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./bottom-bar.component.scss */
      "./src/app/components/bottom-bar/bottom-bar.component.scss")).default]
    })], BottomBarComponent);
    /***/
  },

  /***/
  "./src/app/components/main/main.component.css":
  /*!****************************************************!*\
    !*** ./src/app/components/main/main.component.css ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsMainMainComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".wallet-img {\r\n  border-radius: 50%;\r\n}\r\n\r\n.logo-img {\r\n  border-radius: 8px;\r\n  width: 25px;\r\n  height: 20px;\r\n}\r\n\r\nmat-chip {\r\n  max-width: 200px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tYWluL21haW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tYWluL21haW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53YWxsZXQtaW1nIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuXHJcbi5sb2dvLWltZyB7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIHdpZHRoOiAyNXB4O1xyXG4gIGhlaWdodDogMjBweDtcclxufVxyXG5cclxubWF0LWNoaXAge1xyXG4gIG1heC13aWR0aDogMjAwcHg7XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/components/main/main.component.ts":
  /*!***************************************************!*\
    !*** ./src/app/components/main/main.component.ts ***!
    \***************************************************/

  /*! exports provided: MainComponent */

  /***/
  function srcAppComponentsMainMainComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainComponent", function () {
      return MainComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var ngx_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-clipboard */
    "./node_modules/ngx-clipboard/fesm2015/ngx-clipboard.js");
    /* harmony import */


    var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/snack-bar */
    "./node_modules/@angular/material/esm2015/snack-bar.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var _services_location_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/location.service */
    "./src/app/services/location.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var MainComponent =
    /*#__PURE__*/
    function () {
      function MainComponent(_clipboardService, _snackBar, walletsvc, locSvc, translator) {
        _classCallCheck(this, MainComponent);

        this._clipboardService = _clipboardService;
        this._snackBar = _snackBar;
        this.walletsvc = walletsvc;
        this.locSvc = locSvc;
        this.translator = translator;
      }

      _createClass(MainComponent, [{
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.walletSub.unsubscribe();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this6 = this;

          this.locSvc.getPosition().then(function (result) {
            console.log(result);
          });
          this.walletSub = this.walletsvc.getWalletByEmail().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (changes) {
            return changes.map(function (c) {
              return Object.assign({
                key: c.payload.key
              }, c.payload.val());
            });
          })).subscribe(function (wallet) {
            console.log(wallet[0].address);
            _this6.libraAccount = wallet[0].address;
            _this6.firstName = wallet[0].firstName;

            _this6.getBalance();
          });
        }
      }, {
        key: "getBalance",
        value: function getBalance() {
          var _this7 = this;

          console.log;
          var p = {
            address: this.libraAccount
          };
          this.walletsvc.getBalance(p).then(function (result) {
            _this7.amount = result;
            console.log(_this7.amount);
          });
        }
      }, {
        key: "copyAccount",
        value: function copyAccount() {
          var _this8 = this;

          this.translator.get('Copied').subscribe(function (res) {
            var arrResult = res.split('|');

            _this8._clipboardService.copyFromContent(_this8.libraAccount);

            _this8._snackBar.open(arrResult[0], arrResult[1], {
              duration: 1000
            });
          });
        }
      }, {
        key: "refreshAmount",
        value: function refreshAmount() {
          console.log("refresh balance ...");
          this.getBalance();
        }
      }]);

      return MainComponent;
    }();

    MainComponent.ctorParameters = function () {
      return [{
        type: ngx_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardService"]
      }, {
        type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_4__["WalletService"]
      }, {
        type: _services_location_service__WEBPACK_IMPORTED_MODULE_5__["LocationService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"]
      }];
    };

    MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-main",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./main.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/main/main.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./main.component.css */
      "./src/app/components/main/main.component.css")).default]
    })], MainComponent);
    /***/
  },

  /***/
  "./src/app/components/receive/receive.component.css":
  /*!**********************************************************!*\
    !*** ./src/app/components/receive/receive.component.css ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsReceiveReceiveComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".example-card {\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n          flex-direction: column;\r\n}\r\n\r\n.example-header-image {\r\n  background-size: cover;\r\n}\r\n\r\n.center {\r\n  width: 100px;\r\n  height: 100px;\r\n  position: absolute;\r\n  left: 50%;\r\n  margin-left: -50px;\r\n}\r\n\r\n.wrap {\r\n  word-wrap: break-word;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWNlaXZlL3JlY2VpdmUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0Usc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsU0FBUztFQUNULGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLHFCQUFxQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVjZWl2ZS9yZWNlaXZlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jYXJkIHtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4uZXhhbXBsZS1oZWFkZXItaW1hZ2Uge1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuXHJcbi5jZW50ZXIge1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiA1MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IC01MHB4O1xyXG59XHJcblxyXG4ud3JhcCB7XHJcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG59XHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/components/receive/receive.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/components/receive/receive.component.ts ***!
    \*********************************************************/

  /*! exports provided: ReceiveComponent */

  /***/
  function srcAppComponentsReceiveReceiveComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReceiveComponent", function () {
      return ReceiveComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var ngx_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-clipboard */
    "./node_modules/ngx-clipboard/fesm2015/ngx-clipboard.js");
    /* harmony import */


    var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/snack-bar */
    "./node_modules/@angular/material/esm2015/snack-bar.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ReceiveComponent =
    /*#__PURE__*/
    function () {
      function ReceiveComponent(_clipboardService, _snackBar, translator, walletsvc) {
        _classCallCheck(this, ReceiveComponent);

        this._clipboardService = _clipboardService;
        this._snackBar = _snackBar;
        this.translator = translator;
        this.walletsvc = walletsvc;
      }

      _createClass(ReceiveComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this9 = this;

          this.walletsvc.getWalletByEmail().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (changes) {
            return changes.map(function (c) {
              return Object.assign({
                key: c.payload.key
              }, c.payload.val());
            });
          })).subscribe(function (wallet) {
            _this9.value = wallet[0].address;
          });
        }
      }, {
        key: "copyToClipboard",
        value: function copyToClipboard() {
          var _this10 = this;

          this.translator.get('Copied').subscribe(function (res) {
            var arrResult = res.split('|');

            _this10._clipboardService.copyFromContent(_this10.value);

            _this10._snackBar.open(arrResult[0], arrResult[1], {
              duration: 3000
            });
          });
        }
      }, {
        key: "rotateKey",
        value: function rotateKey() {}
      }]);

      return ReceiveComponent;
    }();

    ReceiveComponent.ctorParameters = function () {
      return [{
        type: ngx_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardService"]
      }, {
        type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_5__["WalletService"]
      }];
    };

    ReceiveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-receive',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./receive.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/receive/receive.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./receive.component.css */
      "./src/app/components/receive/receive.component.css")).default]
    })], ReceiveComponent);
    /***/
  },

  /***/
  "./src/app/components/scan/scan.component.css":
  /*!****************************************************!*\
    !*** ./src/app/components/scan/scan.component.css ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsScanScanComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2Nhbi9zY2FuLmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/components/scan/scan.component.ts":
  /*!***************************************************!*\
    !*** ./src/app/components/scan/scan.component.ts ***!
    \***************************************************/

  /*! exports provided: ScanComponent */

  /***/
  function srcAppComponentsScanScanComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScanComponent", function () {
      return ScanComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _zxing_library__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @zxing/library */
    "./node_modules/@zxing/library/esm5/index.js");
    /* harmony import */


    var _zxing_library__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(_zxing_library__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");

    var ScanComponent =
    /*#__PURE__*/
    function () {
      function ScanComponent(router, walletsvc) {
        _classCallCheck(this, ScanComponent);

        this.router = router;
        this.walletsvc = walletsvc;
        this.currentDevice = null;
        this.formatsEnabled = [_zxing_library__WEBPACK_IMPORTED_MODULE_2__["BarcodeFormat"].CODE_128, _zxing_library__WEBPACK_IMPORTED_MODULE_2__["BarcodeFormat"].DATA_MATRIX, _zxing_library__WEBPACK_IMPORTED_MODULE_2__["BarcodeFormat"].EAN_13, _zxing_library__WEBPACK_IMPORTED_MODULE_2__["BarcodeFormat"].QR_CODE];
        this.torchEnabled = true;
        this.scannerEnabled = true;
        this.torchAvailable$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](false);
        this.tryHarder = false;
      }

      _createClass(ScanComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "clearResult",
        value: function clearResult() {
          this.qrResultString = null;
        }
      }, {
        key: "onCamerasFound",
        value: function onCamerasFound(devices) {
          this.availableDevices = devices;
          this.hasDevices = Boolean(devices && devices.length);
        }
      }, {
        key: "onCodeResult",
        value: function onCodeResult(resultString) {
          this.qrResultString = resultString;
          console.log(this.qrResultString);
          this.walletsvc.setScanAddress(this.qrResultString);
          this.router.navigate(["/send"]);
        }
      }, {
        key: "onDeviceSelectChange",
        value: function onDeviceSelectChange(selected) {
          var device = this.availableDevices.find(function (x) {
            return x.deviceId === selected;
          });
          this.currentDevice = device || null;
        }
      }, {
        key: "onHasPermission",
        value: function onHasPermission(has) {
          this.hasPermission = has;
        }
      }, {
        key: "onTorchCompatible",
        value: function onTorchCompatible(isCompatible) {
          this.torchAvailable$.next(isCompatible || false);
        }
      }, {
        key: "toggleTorch",
        value: function toggleTorch() {
          this.torchEnabled = !this.torchEnabled;
        }
      }, {
        key: "toggleTryHarder",
        value: function toggleTryHarder() {
          this.tryHarder = !this.tryHarder;
        }
      }]);

      return ScanComponent;
    }();

    ScanComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_5__["WalletService"]
      }];
    };

    ScanComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-scan',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./scan.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/scan/scan.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./scan.component.css */
      "./src/app/components/scan/scan.component.css")).default]
    })], ScanComponent);
    /***/
  },

  /***/
  "./src/app/components/send/confirm.component.css":
  /*!*******************************************************!*\
    !*** ./src/app/components/send/confirm.component.css ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsSendConfirmComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VuZC9jb25maXJtLmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/components/send/confirm.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/components/send/confirm.component.ts ***!
    \******************************************************/

  /*! exports provided: ConfirmComponent */

  /***/
  function srcAppComponentsSendConfirmComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfirmComponent", function () {
      return ConfirmComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var ng_animate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ng-animate */
    "./node_modules/ng-animate/fesm2015/ng-animate.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var ConfirmComponent =
    /*#__PURE__*/
    function () {
      function ConfirmComponent(router, walletsvc, translator) {
        _classCallCheck(this, ConfirmComponent);

        this.router = router;
        this.walletsvc = walletsvc;
        this.translator = translator;
        this.enterAnimation = {
          animation: ng_animate__WEBPACK_IMPORTED_MODULE_4__["rotateIn"],
          delay: 50,
          type: "letter"
        };
      }

      _createClass(ConfirmComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this11 = this;

          this.routeData = this.walletsvc.confirmation;
          console.log(this.routeData);
          this.translator.get('Libra sent.').subscribe(function (res) {
            _this11.message = "".concat(_this11.routeData.amount, " ").concat(res);
          });
        }
      }, {
        key: "navigateToHistory",
        value: function navigateToHistory() {
          this.router.navigate(["/history"]);
        }
      }]);

      return ConfirmComponent;
    }();

    ConfirmComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_3__["WalletService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]
      }];
    };

    ConfirmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-confirm",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./confirm.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/confirm.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./confirm.component.css */
      "./src/app/components/send/confirm.component.css")).default]
    })], ConfirmComponent);
    /***/
  },

  /***/
  "./src/app/components/send/send.component.css":
  /*!****************************************************!*\
    !*** ./src/app/components/send/send.component.css ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsSendSendComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".example-container {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n            flex-direction: column;\r\n  }\r\n  \r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZW5kL3NlbmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0VBQ3hCOztBQUVGO0lBQ0ksV0FBVztBQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zZW5kL3NlbmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICB9XHJcbiAgXHJcbi5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/components/send/send.component.ts":
  /*!***************************************************!*\
    !*** ./src/app/components/send/send.component.ts ***!
    \***************************************************/

  /*! exports provided: SendComponent */

  /***/
  function srcAppComponentsSendSendComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendComponent", function () {
      return SendComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var SendComponent =
    /*#__PURE__*/
    function () {
      function SendComponent(fb, router, walletsvc) {
        _classCallCheck(this, SendComponent);

        this.fb = fb;
        this.router = router;
        this.walletsvc = walletsvc;
        this.isLoading = false;
        this.amount_pattern = /^[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*$/;
        this.sendForm = fb.group({
          destAccount: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
          amount: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.amount_pattern)]]
        });
      }

      _createClass(SendComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var scanAddress = this.walletsvc.scanAddresss;
          console.log("SCAN ADDRESS >>> " + scanAddress);

          if (typeof scanAddress !== 'undefined') {
            this.sendForm.patchValue({
              destAccount: scanAddress
            });
          }
        }
      }, {
        key: "onSend",
        value: function onSend() {
          var _this12 = this;

          this.isLoading = true;
          var destAccount = this.sendForm.get("destAccount").value;
          var amount = this.sendForm.get("amount").value;
          this.walletsvc.getWalletByEmail().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (changes) {
            return changes.map(function (c) {
              return Object.assign({
                key: c.payload.key
              }, c.payload.val());
            });
          })).subscribe(function (wallet) {
            console.log(wallet[0].address);
            var p = {
              fromAddress: wallet[0].address,
              mnemonic: wallet[0].mnemonic,
              toAddress: destAccount,
              amount: amount
            };

            _this12.walletsvc.transfer(p).then(function (result) {
              console.log(result);

              _this12.walletsvc.confirmSent(result);

              _this12.isLoading = false;

              _this12.router.navigate(['/confirm-send']);
            });
          });
        }
      }, {
        key: "fc",
        get: function get() {
          return this.sendForm.controls;
        }
      }]);

      return SendComponent;
    }();

    SendComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_4__["WalletService"]
      }];
    };

    SendComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-send',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./send.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send/send.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./send.component.css */
      "./src/app/components/send/send.component.css")).default]
    })], SendComponent);
    /***/
  },

  /***/
  "./src/app/components/transaction-history/transaction-history.component.css":
  /*!**********************************************************************************!*\
    !*** ./src/app/components/transaction-history/transaction-history.component.css ***!
    \**********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsTransactionHistoryTransactionHistoryComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdHJhbnNhY3Rpb24taGlzdG9yeS90cmFuc2FjdGlvbi1oaXN0b3J5LmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/components/transaction-history/transaction-history.component.ts":
  /*!*********************************************************************************!*\
    !*** ./src/app/components/transaction-history/transaction-history.component.ts ***!
    \*********************************************************************************/

  /*! exports provided: TransactionHistoryComponent */

  /***/
  function srcAppComponentsTransactionHistoryTransactionHistoryComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TransactionHistoryComponent", function () {
      return TransactionHistoryComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_wallet_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/wallet.service */
    "./src/app/services/wallet.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var TransactionHistoryComponent =
    /*#__PURE__*/
    function () {
      function TransactionHistoryComponent(walletsvc) {
        _classCallCheck(this, TransactionHistoryComponent);

        this.walletsvc = walletsvc;
      }

      _createClass(TransactionHistoryComponent, [{
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.walletSub.unsubscribe();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this13 = this;

          this.walletSub = this.walletsvc.getWalletByEmail().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (c) {
              return Object.assign({
                key: c.payload.key
              }, c.payload.val());
            });
          })).subscribe(function (wallet) {
            console.log(wallet[0].address);
            _this13.libraAccount = wallet[0].address;
            var p = {
              address: _this13.libraAccount
            };

            _this13.walletsvc.getTransactionHistory(p).then(function (result) {
              console.log(result);
              _this13.transactionHistory = result;
              console.log(_this13.transactionHistory);
            });
          });
        }
      }]);

      return TransactionHistoryComponent;
    }();

    TransactionHistoryComponent.ctorParameters = function () {
      return [{
        type: _services_wallet_service__WEBPACK_IMPORTED_MODULE_2__["WalletService"]
      }];
    };

    TransactionHistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-transaction-history",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./transaction-history.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/transaction-history/transaction-history.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./transaction-history.component.css */
      "./src/app/components/transaction-history/transaction-history.component.css")).default]
    })], TransactionHistoryComponent);
    /***/
  },

  /***/
  "./src/app/material.module.ts":
  /*!************************************!*\
    !*** ./src/app/material.module.ts ***!
    \************************************/

  /*! exports provided: MaterialModule */

  /***/
  function srcAppMaterialModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MaterialModule", function () {
      return MaterialModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/cdk/drag-drop */
    "./node_modules/@angular/cdk/esm2015/drag-drop.js");
    /* harmony import */


    var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/cdk/scrolling */
    "./node_modules/@angular/cdk/esm2015/scrolling.js");
    /* harmony import */


    var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/cdk/table */
    "./node_modules/@angular/cdk/esm2015/table.js");
    /* harmony import */


    var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/cdk/tree */
    "./node_modules/@angular/cdk/esm2015/tree.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    var MaterialModule = function MaterialModule() {
      _classCallCheck(this, MaterialModule);
    };

    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
      exports: [_angular_cdk_table__WEBPACK_IMPORTED_MODULE_3__["CdkTableModule"], _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_4__["CdkTreeModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["DragDropModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatBadgeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatBottomSheetModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatStepperModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatProgressSpinnerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatRippleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSidenavModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSliderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTreeModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__["ScrollingModule"]]
    })], MaterialModule);
    /***/
  },

  /***/
  "./src/app/models/AuthInfo.ts":
  /*!************************************!*\
    !*** ./src/app/models/AuthInfo.ts ***!
    \************************************/

  /*! exports provided: AuthInfo */

  /***/
  function srcAppModelsAuthInfoTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthInfo", function () {
      return AuthInfo;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var AuthInfo =
    /*#__PURE__*/
    function () {
      function AuthInfo($uid, email) {
        _classCallCheck(this, AuthInfo);

        this.$uid = $uid;
        this.email = email;
      }

      _createClass(AuthInfo, [{
        key: "isLoggedIn",
        value: function isLoggedIn() {
          return !!this.$uid;
        }
      }]);

      return AuthInfo;
    }();
    /***/

  },

  /***/
  "./src/app/services/auth.interceptor.ts":
  /*!**********************************************!*\
    !*** ./src/app/services/auth.interceptor.ts ***!
    \**********************************************/

  /*! exports provided: AuthInterceptor */

  /***/
  function srcAppServicesAuthInterceptorTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function () {
      return AuthInterceptor;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var AuthInterceptor =
    /*#__PURE__*/
    function () {
      function AuthInterceptor() {
        _classCallCheck(this, AuthInterceptor);
      }

      _createClass(AuthInterceptor, [{
        key: "intercept",
        value: function intercept(req, next) {
          req = req.clone({
            setHeaders: {
              "content-type": "application/json"
            }
          });
          return next.handle(req);
        }
      }]);

      return AuthInterceptor;
    }();

    AuthInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], AuthInterceptor);
    /***/
  },

  /***/
  "./src/app/services/auth.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/auth.service.ts ***!
    \******************************************/

  /*! exports provided: AuthService */

  /***/
  function srcAppServicesAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthService", function () {
      return AuthService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _models_AuthInfo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../models/AuthInfo */
    "./src/app/models/AuthInfo.ts");

    var AuthService_1;

    var AuthService = AuthService_1 =
    /*#__PURE__*/
    function () {
      function AuthService(afAuth, snackBar) {
        _classCallCheck(this, AuthService);

        this.afAuth = afAuth;
        this.snackBar = snackBar;
        this.authInfo$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](AuthService_1.UNKNOWN_USER);
        this.authState = null;
      }

      _createClass(AuthService, [{
        key: "loginWithEmail",
        value: function loginWithEmail(email, password) {
          return this.fromFirebaseAuthPromise(this.afAuth.auth.signInWithEmailAndPassword(email, password)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError('login', _models_AuthInfo__WEBPACK_IMPORTED_MODULE_6__["AuthInfo"])));
        }
      }, {
        key: "registerWithEmail",
        value: function registerWithEmail(email, password) {
          return this.fromFirebaseAuthPromise(this.afAuth.auth.createUserWithEmailAndPassword(email, password)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError('register', _models_AuthInfo__WEBPACK_IMPORTED_MODULE_6__["AuthInfo"])));
        }
      }, {
        key: "handleError",
        value: function handleError() {
          var _this14 = this;

          var operation = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'operation';
          var result = arguments.length > 1 ? arguments[1] : undefined;
          return function (error) {
            console.error(JSON.stringify(error));

            var snackBarRef = _this14.snackBar.open(JSON.stringify(error), "Done");

            return rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"].throw(error || 'backend server error');
          };
        }
      }, {
        key: "fromFirebaseAuthPromise",
        value: function fromFirebaseAuthPromise(promise) {
          var _this15 = this;

          var subject = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
          promise.then(function (res) {
            var authInfo = new _models_AuthInfo__WEBPACK_IMPORTED_MODULE_6__["AuthInfo"](_this15.afAuth.auth.currentUser.uid, _this15.afAuth.auth.currentUser.email);

            _this15.authInfo$.next(authInfo);

            subject.next(res);
            subject.complete();
          }, function (err) {
            _this15.authInfo$.error(err);

            subject.error(err);
            subject.complete();
          });
          return subject.asObservable();
        }
      }, {
        key: "setFirebaseTokenToLocalstorage",
        value: function setFirebaseTokenToLocalstorage() {
          var _this16 = this;

          this.afAuth.auth.currentUser.getIdToken().then(function (idToken) {
            _this16.saveToken(idToken, _this16.afAuth.auth.currentUser.email);
          });
        }
      }, {
        key: "getToken",
        value: function getToken() {
          return window.localStorage['firebaseToken'];
        }
      }, {
        key: "getEmail",
        value: function getEmail() {
          return window.localStorage['email'];
        }
      }, {
        key: "saveToken",
        value: function saveToken(token, email) {
          window.localStorage['firebaseToken'] = token;
          window.localStorage['email'] = email;
        }
      }, {
        key: "destroyToken",
        value: function destroyToken() {
          window.localStorage.removeItem('firebaseToken');
          window.localStorage.removeItem('email');
        }
      }]);

      return AuthService;
    }();

    AuthService.UNKNOWN_USER = new _models_AuthInfo__WEBPACK_IMPORTED_MODULE_6__["AuthInfo"](null, null);

    AuthService.ctorParameters = function () {
      return [{
        type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]
      }];
    };

    AuthService = AuthService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], AuthService);
    /***/
  },

  /***/
  "./src/app/services/ble.service.ts":
  /*!*****************************************!*\
    !*** ./src/app/services/ble.service.ts ***!
    \*****************************************/

  /*! exports provided: BleService */

  /***/
  function srcAppServicesBleServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BleService", function () {
      return BleService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _manekinekko_angular_web_bluetooth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @manekinekko/angular-web-bluetooth */
    "./node_modules/@manekinekko/angular-web-bluetooth/fesm2015/manekinekko-angular-web-bluetooth.js");

    var BleService =
    /*#__PURE__*/
    function () {
      function BleService(ble) {
        _classCallCheck(this, BleService);

        this.ble = ble;
      }

      _createClass(BleService, [{
        key: "config",
        value: function config(options) {
          this._config = options;
        }
      }, {
        key: "getDevice",
        value: function getDevice() {
          return this.ble.getDevice$();
        }
      }, {
        key: "stream",
        value: function stream() {
          return this.ble.streamValues$().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(this._config.decoder));
        }
      }, {
        key: "value",
        value: function value() {
          return this.ble.value$({
            service: this._config.service,
            characteristic: this._config.characteristic
          });
        }
      }, {
        key: "disconnectDevice",
        value: function disconnectDevice() {
          this.ble.disconnectDevice();
        }
      }]);

      return BleService;
    }();

    BleService.ctorParameters = function () {
      return [{
        type: _manekinekko_angular_web_bluetooth__WEBPACK_IMPORTED_MODULE_3__["BluetoothCore"]
      }];
    };

    BleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], BleService);
    /***/
  },

  /***/
  "./src/app/services/location.service.ts":
  /*!**********************************************!*\
    !*** ./src/app/services/location.service.ts ***!
    \**********************************************/

  /*! exports provided: LocationService */

  /***/
  function srcAppServicesLocationServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocationService", function () {
      return LocationService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var LocationService =
    /*#__PURE__*/
    function () {
      function LocationService() {
        _classCallCheck(this, LocationService);
      }

      _createClass(LocationService, [{
        key: "getPosition",
        value: function getPosition() {
          return new Promise(function (resolve, reject) {
            navigator.geolocation.getCurrentPosition(function (resp) {
              resolve({
                lng: resp.coords.longitude,
                lat: resp.coords.latitude
              });
            }, function (err) {
              reject(err);
            });
          });
        }
      }]);

      return LocationService;
    }();

    LocationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: "root"
    })], LocationService);
    /***/
  },

  /***/
  "./src/app/services/wallet.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/wallet.service.ts ***!
    \********************************************/

  /*! exports provided: WalletService */

  /***/
  function srcAppServicesWalletServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WalletService", function () {
      return WalletService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./auth.service */
    "./src/app/services/auth.service.ts");

    var WalletService =
    /*#__PURE__*/
    function () {
      function WalletService(http, db, authSvc) {
        _classCallCheck(this, WalletService);

        this.http = http;
        this.db = db;
        this.authSvc = authSvc;
        this.dbPath = "/wallet";
        this.walletRef$ = this.db.list(this.dbPath);
      }

      _createClass(WalletService, [{
        key: "getBalance",
        value: function getBalance(address) {
          console.log(address);
          return this.http.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url, "/getBalance"), address).toPromise();
        }
      }, {
        key: "getTransactionHistory",
        value: function getTransactionHistory(address) {
          console.log(address);
          return this.http.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url, "/transactionHistory"), address).toPromise();
        }
      }, {
        key: "transfer",
        value: function transfer(fund) {
          console.log(fund);
          return this.http.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url, "/transfer"), fund).toPromise();
        }
      }, {
        key: "createWallet",
        value: function createWallet() {
          return this.http.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url, "/createWallet"), {}).toPromise();
        }
      }, {
        key: "saveWallet",
        value: function saveWallet(wallet) {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])(this.walletRef$.push(wallet));
        }
      }, {
        key: "confirmSent",
        value: function confirmSent(p) {
          this.confirmation = p;
        }
      }, {
        key: "setScanAddress",
        value: function setScanAddress(address) {
          this.scanAddresss = address;
        }
      }, {
        key: "getWalletByEmail",
        value: function getWalletByEmail() {
          var _this17 = this;

          this.singleWalletRef$ = this.db.list(this.dbPath, function (ref) {
            return ref.orderByChild("email").equalTo(_this17.authSvc.getEmail().toString());
          });
          return this.singleWalletRef$;
        }
      }]);

      return WalletService;
    }();

    WalletService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]
      }];
    };

    WalletService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], WalletService);
    /***/
  },

  /***/
  "./src/app/shared/localized-date.pipe.ts":
  /*!***********************************************!*\
    !*** ./src/app/shared/localized-date.pipe.ts ***!
    \***********************************************/

  /*! exports provided: LocalizedDatePipe */

  /***/
  function srcAppSharedLocalizedDatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocalizedDatePipe", function () {
      return LocalizedDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var LocalizedDatePipe =
    /*#__PURE__*/
    function () {
      function LocalizedDatePipe(translate) {
        _classCallCheck(this, LocalizedDatePipe);

        this.translate = translate;
      }

      _createClass(LocalizedDatePipe, [{
        key: "transform",
        value: function transform(date) {
          var pattern = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'mediumDate';
          var currentLang = this.translate.currentLang; // if we ask another time for the same date & locale, return the last value

          if (date === this.lastDate && currentLang === this.lastLang) {
            return this.value;
          }

          this.value = new _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"](currentLang).transform(date, pattern);
          this.lastDate = date;
          this.lastLang = currentLang;
          return this.value;
        }
      }]);

      return LocalizedDatePipe;
    }();

    LocalizedDatePipe.ctorParameters = function () {
      return [{
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]
      }];
    };

    LocalizedDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Pipe"])({
      name: 'localizedDate',
      pure: false
    })], LocalizedDatePipe);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      libra_account: 'f4b79df549f1d1922bf2b170b97febc098e3cadef427f3b2646e8bff54cfa92a',
      mnemonic: 'bleak piano damp benefit swim that pass battle exercise maple quarter sausage organ crawl patch episode argue embark biology ostrich claw dad arrive cause',
      api_url: 'https://b1f08b51.jp.ngrok.io',
      firebase: {
        apiKey: "AIzaSyDeca5K26DxHVzrAUpVsEgj2kjTM_7q_SI",
        authDomain: "libra-wallet-28877.firebaseapp.com",
        databaseURL: "https://libra-wallet-28877.firebaseio.com",
        projectId: "libra-wallet-28877",
        storageBucket: "libra-wallet-28877.appspot.com",
        messagingSenderId: "903414187806",
        appId: "1:903414187806:web:4e1f95dad2a1f0b0782b0f"
      }
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! hammerjs */
    "./node_modules/hammerjs/hammer.js");
    /* harmony import */


    var hammerjs__WEBPACK_IMPORTED_MODULE_1___default =
    /*#__PURE__*/
    __webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_4__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! E:\Projects\angular-libra-wallet\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map